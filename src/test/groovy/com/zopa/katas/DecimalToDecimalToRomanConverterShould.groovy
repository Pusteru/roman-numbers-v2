package com.zopa.katas

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

class DecimalToDecimalToRomanConverterShould extends Specification {

    @Subject
    DecimalToRomanConverter romanConverter = new DecimalToRomanConverter()

    @Unroll
    def "convert a #decimalNumber.value to a #romanNumber.value"() {
        when:
        def number = romanConverter.convert(decimalNumber)
        then:
        romanNumber == number
        where:
        decimalNumber           | romanNumber
        new DecimalNumber(1)    | new RomanNumber("I")
        new DecimalNumber(5)    | new RomanNumber("V")
        new DecimalNumber(10)   | new RomanNumber("X")
        new DecimalNumber(50)   | new RomanNumber("L")
        new DecimalNumber(100)  | new RomanNumber("C")
        new DecimalNumber(500)  | new RomanNumber("D")
        new DecimalNumber(1000) | new RomanNumber("M")
    }

    @Unroll
    def "convert a #decimalNumber.value to a repetition of #romanNumber.value"() {
        when:
        def number = romanConverter.convert(decimalNumber)
        then:
        romanNumber == number
        where:
        decimalNumber          | romanNumber
        new DecimalNumber(3)   | new RomanNumber("III")
        new DecimalNumber(30)  | new RomanNumber("XXX")
        new DecimalNumber(300) | new RomanNumber("CCC")
    }

    @Unroll
    def "convert #decimalNumber.value to a combination of #romanNumber.value without subtraction"() {
        when:
        def number = romanConverter.convert(decimalNumber)
        then:
        romanNumber == number
        where:
        decimalNumber          | romanNumber
        new DecimalNumber(11)  | new RomanNumber("XI")
        new DecimalNumber(55)  | new RomanNumber("LV")
        new DecimalNumber(532) | new RomanNumber("DXXXII")
    }

    @Unroll
    def "convert #decimalNumber.value to a #romanNumber.value with a subtraction"() {
        when:
        def number = romanConverter.convert(decimalNumber)
        then:
        romanNumber == number
        where:
        decimalNumber         | romanNumber
        new DecimalNumber(9)  | new RomanNumber("IX")
        new DecimalNumber(4)  | new RomanNumber("IV")
        new DecimalNumber(14) | new RomanNumber("XIV")
        new DecimalNumber(49) | new RomanNumber("XLIX")
        new DecimalNumber(994) | new RomanNumber("CMXCIV")
    }
}
