package com.zopa.katas

import spock.lang.Specification
import spock.lang.Unroll

class RomanToDecimalConverterShould extends Specification {

    def romanToDecimalConverter = new RomanToDecimalConverter()

    def "throw an exception when the number is not correct"() {
        when:
        romanToDecimalConverter.convert(incorrectNumber)
        then:
        thrown(NumberFormatException)
        where:
        incorrectNumber | _
        new RomanNumber("T") | _
        new RomanNumber("2") | _
        new RomanNumber("CIIW") | _
    }

    @Unroll
    def "convert a #romanNumber.value to a #decimalNumber.value"() {
        when:
        def number = romanToDecimalConverter.convert(romanNumber)
        then:
        decimalNumber == number
        where:
        decimalNumber           | romanNumber
        new DecimalNumber(1)    | new RomanNumber("I")
        new DecimalNumber(5)    | new RomanNumber("V")
        new DecimalNumber(10)   | new RomanNumber("X")
        new DecimalNumber(50)   | new RomanNumber("L")
        new DecimalNumber(100)  | new RomanNumber("C")
        new DecimalNumber(500)  | new RomanNumber("D")
        new DecimalNumber(1000) | new RomanNumber("M")
    }


    @Unroll
    def "convert a #romanNumber.value with repetitions to a #decimalNumber.value"() {
        when:
        def number = romanToDecimalConverter.convert(romanNumber)
        then:
        decimalNumber == number
        where:
        decimalNumber          | romanNumber
        new DecimalNumber(3)   | new RomanNumber("III")
        new DecimalNumber(30)  | new RomanNumber("XXX")
        new DecimalNumber(300) | new RomanNumber("CCC")
    }

    @Unroll
    def "convert #romanNumber.value without subtraction to a #decimalNumber.value"() {
        when:
        def number = romanToDecimalConverter.convert(romanNumber)
        then:
        decimalNumber == number
        where:
        decimalNumber          | romanNumber
        new DecimalNumber(11)  | new RomanNumber("XI")
        new DecimalNumber(55)  | new RomanNumber("LV")
        new DecimalNumber(532) | new RomanNumber("DXXXII")
    }

    @Unroll
    def "convert #romanNumber.value with a subtraction to a #decimalNumber.value"() {
        when:
        def number = romanToDecimalConverter.convert(romanNumber)
        then:
        decimalNumber == number
        where:
        decimalNumber          | romanNumber
        new DecimalNumber(9)   | new RomanNumber("IX")
        new DecimalNumber(4)   | new RomanNumber("IV")
        new DecimalNumber(14)  | new RomanNumber("XIV")
        new DecimalNumber(49)  | new RomanNumber("XLIX")
        new DecimalNumber(994) | new RomanNumber("CMXCIV")
    }

}
