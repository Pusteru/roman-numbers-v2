package com.zopa.katas

class RomanToDecimalConverter {

    fun convert(romanNumber: RomanNumber): DecimalNumber{
        if(romanNumber.isEmpty()){
            return DecimalNumber(0)
        }
        val romanNumberPart = RomanDecimalMapping.findRomanNumber(romanNumber)
        return romanNumberPart.decimalNumber.add(convert(romanNumber.subtract(romanNumberPart.romanNumber)))
    }
}