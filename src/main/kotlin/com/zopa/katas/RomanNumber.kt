package com.zopa.katas

data class RomanNumber(private val value: String) {
    fun add(number: RomanNumber): RomanNumber = RomanNumber(number.value + value)

    fun isEmpty(): Boolean = value.isEmpty()

    fun subtract(romanNumberPart: RomanNumber): RomanNumber = RomanNumber(value.removePrefix(romanNumberPart.value))

    fun startsWith(romanNumber: RomanNumber): Boolean = value.startsWith(romanNumber.value)

}