package com.zopa.katas

data class DecimalNumber(var value: Int) {
    fun isZero(): Boolean {
        return value == 0
    }

    fun subtract(closerValue: DecimalNumber): DecimalNumber {
        return DecimalNumber(value - closerValue.value)
    }

    fun add(decimalNumber: DecimalNumber): DecimalNumber {
        return DecimalNumber(value + decimalNumber.value)
    }
}