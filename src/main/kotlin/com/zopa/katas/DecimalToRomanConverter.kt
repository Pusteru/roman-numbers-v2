package com.zopa.katas

class DecimalToRomanConverter {

    fun convert(decimalNumber: DecimalNumber): RomanNumber {
        if(decimalNumber.isZero()) {
            return RomanNumber("")
        }
        val closerValue = RomanDecimalMapping.findCloserDecimal(decimalNumber)
        return convert(decimalNumber.subtract(closerValue.decimalNumber)).add(closerValue.romanNumber)
    }
}