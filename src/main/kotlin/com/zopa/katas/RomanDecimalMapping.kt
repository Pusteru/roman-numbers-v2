package com.zopa.katas

import java.lang.NumberFormatException

enum class RomanDecimalMapping constructor(val decimalNumber: DecimalNumber,
                                           val romanNumber: RomanNumber) {
    THOUSAND(DecimalNumber(1000),RomanNumber("M")),
    NINE_HUNDRED(DecimalNumber(900),RomanNumber("CM")),
    FIVE_HUNDRED(DecimalNumber(500),RomanNumber("D")),
    FOUR_HUNDRED(DecimalNumber(400),RomanNumber("CD")),
    HUNDRED(DecimalNumber(100),RomanNumber("C")),
    NINETY(DecimalNumber(90),RomanNumber("XC")),
    FIFTY(DecimalNumber(50),RomanNumber("L")),
    FORTY(DecimalNumber(40),RomanNumber("XL")),
    TEN(DecimalNumber(10),RomanNumber("X")),
    NINE(DecimalNumber(9),RomanNumber("IX")),
    FIVE(DecimalNumber(5),RomanNumber("V")),
    FOUR(DecimalNumber(4),RomanNumber("IV")),
    ONE(DecimalNumber(1),RomanNumber("I"));

    companion object {
        fun findCloserDecimal(decimalNumber: DecimalNumber): RomanDecimalMapping {
            return values().first { decimalNumber.value >= it.decimalNumber.value }
        }

        fun findRomanNumber(romanNumber: RomanNumber): RomanDecimalMapping {
            return values().firstOrNull { romanNumber.startsWith(it.romanNumber) } ?: throw NumberFormatException()
        }
    }

}